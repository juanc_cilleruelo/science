unit Scene3DForm;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Rtti, System.Classes,
  System.Variants, FMX.Types, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.Types3D,
  FMX.Objects3D, FMX.Materials, FMX.Edit, FMX.ListBox, FMX.Objects, System.Math.Vectors,
  FMX.StdCtrls, FMX.Controls.Presentation, FMX.MaterialSources, FMX.Controls3D, FMX.Viewport3D,
  {Project Classes}
  MeshObjects;

type
  Tfrm3dScene = class(TForm)
    CamDummy1  :TDummy;
    CamDummy2  :TDummy;
    DummyStart :TDummy;

    MyCamera   :TCamera;
    Scene3d    :TViewport3D;
    UtilsPanel :TPanel;
    MainGrid   :TGrid3D;
    BtnAnnulus :TButton;
    MainStBar  :TStatusBar;
    StLabel    :TLabel;
    camLight   :TLight;
    CheckBoxAnnulusOutRect: TCheckBox;
    Label1: TLabel;
    LightMatSrc1: TLightMaterialSource;
    LightMatSrc2: TLightMaterialSource;
    TextureMatSrc1: TTextureMaterialSource;
    TextureMatSrc2: TTextureMaterialSource;
    CheckBoxAnnulusUseTexture: TCheckBox;
    TrackBarAnnulusThickness: TTrackBar;
    BtnPipe: TButton;
    CheckBoxAnnulusInnerRect: TCheckBox;
    CheckBoxPipeUseTexture: TCheckBox;
    CheckBoxPipeOuterRect: TCheckBox;
    CheckBoxPipeInnerRect: TCheckBox;
    Label2: TLabel;
    TrackBarPipeThickness: TTrackBar;
    Line1: TLine;
    Label3: TLabel;
    ComboBoxSection: TComboBox;
    ListBoxItem1: TListBoxItem;
    ListBoxItem2: TListBoxItem;
    ListBoxItem3: TListBoxItem;
    TrackBarSectionDegree: TTrackBar;
    Label4: TLabel;
    BtnAddTwist: TButton;
    BtnAddEmboss: TButton;
    BtnAddBend: TButton;
    BtnAddBendTurned: TButton;
    BtnAddSecondBend: TButton;
    BtnClearAllModifiers: TButton;
    procedure Scene3dMouseWheel(Sender: TObject; Shift: TShiftState; WheelDelta: Integer; var Handled: Boolean);
    procedure Scene3dMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Single);
    procedure Scene3dMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Single);
    procedure Scene3dMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Single);
    procedure BtnAnnulusClick(Sender: TObject);
    procedure CheckBoxAnnulusOutRectChange(Sender: TObject);
    procedure CheckBoxAnnulusUseTextureChange(Sender: TObject);
    procedure TrackBarAnnulusThicknessChange(Sender: TObject);
    procedure CheckBoxAnnulusInnerRectChange(Sender: TObject);
    procedure BtnPipeClick(Sender: TObject);
    procedure CheckBoxPipeUseTextureChange(Sender: TObject);
    procedure CheckBoxPipeOuterRectChange(Sender: TObject);
    procedure CheckBoxPipeInnerRectChange(Sender: TObject);
    procedure TrackBarPipeThicknessChange(Sender: TObject);
    procedure ComboBoxSectionChange(Sender: TObject);
    procedure TrackBarSectionDegreeChange(Sender: TObject);
    procedure BtnAddTwistClick(Sender: TObject);
    procedure BtnAddEmbossClick(Sender: TObject);
    procedure BtnAddBendClick(Sender: TObject);
    procedure BtnAddBendTurnedClick(Sender: TObject);
    procedure BtnAddSecondBendClick(Sender: TObject);
    procedure BtnClearAllModifiersClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    FISPressed :Boolean        ;
    FOldPoint  :TPointF        ;
    Annulus    :TAnnulus       ;
    Pipe       :TPipe          ;
    Twister    :TTwistModifier ;
    Bender1    :TBendModifier  ;
    Bender2    :TBendModifier  ;
    Emboss     :TEmbossModifier;
  public
    procedure ClearScene;
  end;

var
  frm3dScene :Tfrm3dScene;

implementation

{$R *.fmx}

procedure Tfrm3dScene.FormCreate(Sender: TObject);
var Rectangle :TRectangle3D;
begin
   FISPressed := False;

   //FOldPoint  := nil;
   Annulus    := nil;
   Pipe       := nil;

   Twister := nil;
   Bender1 := nil;
   Bender2 := nil;
   Emboss  := nil;


   Rectangle := TRectangle3D.Create(Self);
   Rectangle.Width  := 10;
   Rectangle.Height := 10;
   Rectangle.Depth  := 1;
   Rectangle.Position := TPosition3D.Create(Point3D(10, 0, 18));
   Rectangle.MaterialSource := LightMatSrc2;
   DummyStart.AddObject(Rectangle);
end;

procedure Tfrm3dScene.BtnAnnulusClick(Sender: TObject);
begin
   ClearScene;

   TrackBarAnnulusThickness.Enabled    := True;
   CheckBoxAnnulusOutRect.Enabled      := True;
   CheckBoxAnnulusUseTexture.Enabled   := True;
   CheckBoxAnnulusInnerRect.Enabled    := True;

   TrackBarAnnulusThickness.Value      := 20;
   CheckBoxAnnulusOutRect.IsChecked    := False;
   CheckBoxAnnulusUseTexture.IsChecked := False;
   CheckBoxAnnulusInnerRect.IsChecked  := False;

   Annulus := TAnnulus.Create(Self);
   Annulus.Width          := 1;
   Annulus.Depth          := 1;
   Annulus.Thickness      := 0.2;
   Annulus.MaterialSource := LightMatSrc1;
   DummyStart.AddObject(Annulus);

   CamDummy1.AnimateFloat('RotationAngle.Z',  10);
   CamDummy2.AnimateFloat('RotationAngle.X', -40);
   MyCamera.AnimateFloat('Position.Z', 10);
end;

procedure Tfrm3dScene.BtnPipeClick(Sender: TObject);
begin
   ClearScene;
   TrackBarPipeThickness.Enabled    := True;
   CheckBoxPipeUseTexture.Enabled   := True;
   CheckBoxPipeOuterRect.Enabled    := True;
   CheckBoxPipeInnerRect.Enabled    := True;
   ComboBoxSection.Enabled          := True;
   TrackBarSectionDegree.Enabled    := True;
   TrackBarPipeThickness.Value      := 20;

   CheckBoxPipeUseTexture.IsChecked := False;
   CheckBoxPipeOuterRect.IsChecked  := False;
   CheckBoxPipeInnerRect.IsChecked  := False;
   TrackBarSectionDegree.Value      := 45;
   ComboBoxSection.ItemIndex        := 0;

   BtnAddTwist.Enabled              := True;
   BtnAddEmboss.Enabled             := True;
   BtnAddBend.Enabled               := True;
   BtnAddSecondBend.Enabled         := True;
   BtnAddBendTurned.Enabled         := True;
   BtnClearAllModifiers.Enabled     := True;

   Pipe := TPipe.Create(Self);
   Pipe.Width          := 1;
   Pipe.Depth          := 1;
   Pipe.Height         := 8;
   Pipe.Thickness      := 0.2;
   Pipe.SectionType    := TSectionType.sctNone;
   Pipe.SectionDegree  := 45;
   Pipe.MaterialSource := LightMatSrc1;
   DummyStart.AddObject(Pipe);

   CamDummy1.AnimateFloat('RotationAngle.Z',  10);
   CamDummy2.AnimateFloat('RotationAngle.X', -40);
   MyCamera.AnimateFloat('Position.Z', 10);
end;

procedure Tfrm3dScene.BtnAddTwistClick(Sender: TObject);
begin
   if Assigned(Pipe) then begin
      if Pipe.OuterFrameType = ftEllipse then begin
        ShowMessage('You can see twist effect best on rectangular pipe. Set the outer frame type to Rectangle!');
      end;

      if assigned(Twister) then begin
         Pipe.Modifiers.Remove(Twister);
         Twister.Free;
      end;

      Twister := TTwistModifier.Create(Pipe);
      Twister.StartPosition :=   5;
      Twister.EndPosition   :=   8;
      Twister.TotalRotation := 360;
      Twister.Subdivisions  :=   4;
      Pipe.Modifiers.Add(Twister);
      Pipe.Repaint;
   end;
end;

procedure Tfrm3dScene.BtnAddEmbossClick(Sender: TObject);
begin
   if assigned(Pipe) then begin
      if Assigned(Emboss) then begin
         Pipe.Modifiers.Remove(Emboss);
         Emboss.Free;
      end;

      Emboss := TEmbossModifier.Create(Pipe);
      Emboss.StartPosition  := 0.5;
      Emboss.EndPosition    := 1.0;
      Emboss.ThicknessRatio := 0.1;
      Pipe.Modifiers.Add(Emboss);
      Pipe.Repaint;
   end;
end;

procedure Tfrm3dScene.BtnAddBendClick(Sender: TObject);
begin
   if Assigned(Pipe) then begin
      if Assigned(Bender1) then begin
         Pipe.Modifiers.Remove(Bender1);
         Bender1.Free;
      end;
      Bender1 := TBendModifier.Create(Pipe);
      Bender1.StartPosition := 1.0;
      Bender1.EndPosition   := 2.5;
      Bender1.BendAngle     := 90;
      Bender1.Subdivisions  := 30;
      Pipe.Modifiers.Add(Bender1);
      Pipe.Repaint;
   end;
end;

procedure Tfrm3dScene.BtnAddBendTurnedClick(Sender: TObject);
begin
   if assigned(Pipe) then begin
      if assigned(Bender1) then begin
         Pipe.Modifiers.Remove(Bender1);
         Bender1.Free;
      end;
      Bender1 := TBendModifier.Create(Pipe);
      Bender1.StartPosition := 1.0;
      Bender1.EndPosition   := 2.5;
      Bender1.BendAngle     := 90;
      Bender1.TurnAngle     := 90;
      Bender1.Subdivisions  := 30;
      Pipe.Modifiers.Add(Bender1);
      Pipe.Repaint;
   end;
end;

procedure Tfrm3dScene.BtnAddSecondBendClick(Sender: TObject);
begin
   if Assigned(Pipe) then begin
      if Assigned(Bender2) then begin
        Pipe.Modifiers.Remove(Bender2);
        Bender2.Free;
      end;
      Bender2 := TBendModifier.Create(Pipe);
      Bender2.StartPosition := 2.5;
      Bender2.EndPosition   := 4;
      Bender2.BendAngle     := -90;
      Pipe.Modifiers.Add(Bender2);
      Pipe.Repaint;
   end;
end;

procedure Tfrm3dScene.BtnClearAllModifiersClick(Sender: TObject);
begin
   if Assigned(Pipe) then begin
      Pipe.ClearModifiers;
      Twister := nil;
      Bender1 := nil;
      Bender2 := nil;
      Emboss  := nil;
      Pipe.Repaint;
   end;
end;

procedure Tfrm3dScene.CheckBoxAnnulusOutRectChange(Sender: TObject);
begin
   if assigned(Annulus) then begin
      if CheckBoxAnnulusOutRect.IsChecked then begin
         Annulus.OuterFrameType := TFrameType.ftRectangle;
      end
      else begin
         Annulus.OuterFrameType := TFrameType.ftEllipse;
      end;
      Annulus.Repaint;
   end;
end;

procedure Tfrm3dScene.CheckBoxAnnulusUseTextureChange(Sender: TObject);
begin
  if assigned(Annulus) then begin
    if CheckBoxAnnulusUseTexture.IsChecked then begin
      Annulus.MaterialSource := TextureMatSrc1;
    end
    else begin
      Annulus.MaterialSource := LightMatSrc1;
    end;
    Annulus.Repaint;
  end;
end;

procedure Tfrm3dScene.CheckBoxAnnulusInnerRectChange(Sender: TObject);
begin
   if Assigned(Annulus) then begin
      if CheckBoxAnnulusInnerRect.IsChecked then begin
         Annulus.InnerFrameType := TFrameType.ftRectangle;
      end
      else begin
         Annulus.InnerFrameType := TFrameType.ftEllipse;
      end;
      Annulus.Repaint;
   end;
end;

procedure Tfrm3dScene.CheckBoxPipeUseTextureChange(Sender: TObject);
begin
   if Assigned(Pipe) then begin
      if CheckBoxPipeUseTexture.IsChecked then begin
         Pipe.MaterialSource := TextureMatSrc2;
      end
      else begin
         Pipe.MaterialSource := LightMatSrc1;
      end;
      Pipe.Repaint;
   end;
end;

procedure Tfrm3dScene.CheckBoxPipeOuterRectChange(Sender: TObject);
begin
   if Assigned(Pipe) then begin
      if CheckBoxPipeOuterRect.IsChecked then begin
         Pipe.OuterFrameType := TFrameType.ftRectangle;
      end
      else begin
         Pipe.OuterFrameType := TFrameType.ftEllipse;
      end;
      Pipe.Repaint;
   end;
end;

procedure Tfrm3dScene.CheckBoxPipeInnerRectChange(Sender: TObject);
begin
   if assigned(Pipe) then begin
      if CheckBoxPipeInnerRect.IsChecked then begin
         Pipe.InnerFrameType := TFrameType.ftRectangle;
      end
      else begin
         Pipe.InnerFrameType := TFrameType.ftEllipse;
      end;
      Pipe.Repaint;
   end;
end;

procedure Tfrm3dScene.ClearScene;
begin
   DummyStart.DeleteChildren;
   Annulus                           := nil;
   Pipe                              := nil;
   Twister                           := nil;
   Bender1                           := nil;
   Bender2                           := nil;
   Emboss                            := nil;
   TrackBarAnnulusThickness.Enabled  := False;
   CheckBoxAnnulusOutRect.Enabled    := False;
   CheckBoxAnnulusUseTexture.Enabled := False;
   CheckBoxAnnulusInnerRect.Enabled  := False;
   TrackBarPipeThickness.Enabled     := False;
   CheckBoxPipeUseTexture.Enabled    := False;
   CheckBoxPipeOuterRect.Enabled     := False;
   CheckBoxPipeInnerRect.Enabled     := False;
   ComboBoxSection.Enabled           := False;
   TrackBarSectionDegree.Enabled     := False;
   BtnAddTwist.Enabled               := False;
   BtnAddEmboss.Enabled              := False;
   BtnAddBend.Enabled                := False;
   BtnAddSecondBend.Enabled          := False;
   BtnAddBendTurned.Enabled          := False;
   BtnClearAllModifiers.Enabled      := False;
end;

procedure Tfrm3dScene.ComboBoxSectionChange(Sender: TObject);
begin
   if Assigned(Pipe) then begin
      Pipe.SectionType := TSectionType(ComboBoxSection.ItemIndex);
      Pipe.Repaint;
   end;
end;

procedure Tfrm3dScene.Scene3dMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Single);
begin
   FIsPressed := True;
   FOldPoint  := PointF(X,Y);
end;

procedure Tfrm3dScene.Scene3dMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Single);
begin
   if FIsPressed then begin
      CamDummy1.RotationAngle.Z := CamDummy1.RotationAngle.Z + (X-FOldPoint.X)*0.5;
      CamDummy2.RotationAngle.X := CamDummy2.RotationAngle.X + (Y-FOldPoint.Y)*0.5;
      FOldPoint := PointF(X,Y);
   end;
end;

procedure Tfrm3dScene.Scene3dMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Single);
begin
   FIsPressed := False;
end;

procedure Tfrm3dScene.Scene3dMouseWheel(Sender: TObject; Shift: TShiftState; WheelDelta: Integer; var Handled: Boolean);
begin
  //MyCamera.Position.Vector := MyCamera.Position.Vector + Vector3D(0, 0, 1).Scale((WheelDelta / 40) * 0.8);
end;

procedure Tfrm3dScene.TrackBarAnnulusThicknessChange(Sender: TObject);
begin
   if Assigned(Annulus) then begin
      Annulus.Thickness := TrackBarAnnulusThickness.Value/100;
      Annulus.Repaint;
   end;
end;

procedure Tfrm3dScene.TrackBarPipeThicknessChange(Sender: TObject);
begin
   if Assigned(Pipe) then begin
      Pipe.Thickness := TrackBarPipeThickness.Value/100;
      Pipe.Repaint;
   end;
end;

procedure Tfrm3dScene.TrackBarSectionDegreeChange(Sender: TObject);
begin
   if Assigned(Pipe) then begin
      Pipe.SectionDegree := Round(TrackBarSectionDegree.Value);
      Pipe.Repaint;
   end;
end;

end.
